$(function () {
	var $mapWr = $(".js-map");
	if(!$mapWr.length) return;

	var $mapBtn = $mapWr.find(".js-map-btn"), 
		$mapContainer = $mapWr.find(".js-map-container"),
		$mapBtnText =  $mapBtn.find(".js-map-btn-text"), 
		mapBtnTextOpen = $mapBtnText.data("text-open"),
		mapBtnTextClose = $mapBtnText.data("text-close");

	$mapBtn.click(function(){

		$mapContainer.toggleClass('_open');
		$mapBtn.toggleClass('_open');

		if ($mapBtn.hasClass('_open')) {
			$mapBtnText.text(mapBtnTextOpen);
		} else {
			$mapBtnText.text(mapBtnTextClose);
		}

		// $mapBtnText.text($mapBtnText.text() == mapBtnTextOpen ? mapBtnTextClose : mapBtnTextOpen); 
	    return false;
	});
});

$(function(){
	var $menuWr = $(".js-menu-wr");
	if(!$menuWr.length) return;

	var $menuBtn = $menuWr.find(".js-menu-btn"), 
		$menu = $menuWr.find(".js-menu");

	$menuBtn.click(function(){
		$menu.slideToggle();
	});
});

$(function(){
	var $jsTabsWr = $('.js-tabs-wr ');
	if(!$jsTabsWr.length) return;

	var $jsTabs = $jsTabsWr.find('.js-tabs a'),
		$jsTabCont = $jsTabsWr.find('.js-tab-cont > div');

    $jsTabs.click(function(){
	    $jsTabCont.addClass('hide');
	    $jsTabs.parent().removeClass('active');
	    
	    var id = $(this).attr('href');
	    $(id).removeClass('hide');
	    $(this).parent().addClass('active');
	    return false;
  });
});

$(function(){
	var $jsTabsContainer = $('.js-tab-container ');
	if(!$jsTabsContainer.length) return;

	var $jsSliderTabs = $jsTabsContainer.find('.js-registration-slider a'),
		$jsTablesCont = $jsTabsContainer.find('.js-tables-cont > div');

    $jsSliderTabs.click(function(){
	    $jsTablesCont.addClass('hide');
	    $jsSliderTabs.removeClass('active');
	    
	    var id = $(this).attr('href');
	    $(id).removeClass('hide');
	    $(this).addClass('active');
	    return false;
  });
});

$(function(){
	var $jsTabsWrap = $('.js-tabs-wrap ');
	if(!$jsTabsWrap.length) return;

	var $jsTabList = $jsTabsWrap.find('.js-tab-list li'),
		$jsTabContainer = $jsTabsWrap.find('.js-tab-container > div');

    $jsTabList.click(function(){
    	 var id = $(this).index();
	   
	    $jsTabContainer.addClass('hide').eq(id).removeClass('hide');
	    $jsTabList.removeClass('active').eq(id).addClass('active');

	    $jsTabsWrap.find(':not(.hide) .js-registration-slider').slick('setPosition');
	    
	    
	    return false;
  });
});

$(function () {
	var $registrationSlider = $('.js-registration-slider');
	if(!$registrationSlider.length) return;

	$registrationSlider.slick({
		arrows: false,
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 6,
		slidesToScroll: 3,
		responsive: [
		{
		breakpoint: 1000,
		settings: {
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  infinite: true,
		  dots: true
		}
		},
		{
		breakpoint: 768,
		settings: {
		  slidesToShow: 2,
		  slidesToScroll: 1,
		  dots: true
		}
		},
		{
		breakpoint: 480,
		settings: {
		  centerMode: true,
		  centerPadding: '62px',
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  dots: true
		}
		}
		]
	});
});
